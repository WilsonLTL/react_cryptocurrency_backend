const path = require('path');
const root = path.parse(process.mainModule.filename).dir;
const express = require('express');
const router = express.Router({ mergeParams: true });

const handler = {
    btc_usd: require("./handlers/btc_usd").btc_usd,
    eth_usd: require("./handlers/eth_usd").eth_usd,
    ltc_usd: require("./handlers/ltc_usd").ltc_usd,
    xmr_usd: require("./handlers/xmr_usd").xmr_usd,
    xrp_usd: require("./handlers/xrp_usd").xrp_usd,
    doge_usd: require("./handlers/doge_usd").doge_usd,
    dash_usd: require("./handlers/dash_usd").dash_usd,
    maid_usd: require("./handlers/maid_usd").maid_usd,
    lsk_usd: require("./handlers/lsk_usd").lsk_usd,
    sjcx_usd: require("./handlers/sjcx_usd").sjcx_usd,
    update_data: require("./handlers/update_data").update_data,
    push_message: require("./handlers/push_alert_message").push_message,
};

router.route("/btc_usd").post(handler.btc_usd);
router.route("/eth_usd").post(handler.eth_usd);
router.route("/ltc_usd").post(handler.ltc_usd);
router.route("/xmr_usd").post(handler.xmr_usd);
router.route("/xrp_usd").post(handler.xrp_usd);
router.route("/doge_usd").post(handler.doge_usd);
router.route("/dash_usd").post(handler.dash_usd);
router.route("/maid_usd").post(handler.maid_usd);
router.route("/lsk_usd").post(handler.lsk_usd);
router.route("/sjcx_usd").post(handler.sjcx_usd);
router.route("/update_data").post(handler.update_data);
router.route("/push_message").post(handler.push_message);

module.exports = router;