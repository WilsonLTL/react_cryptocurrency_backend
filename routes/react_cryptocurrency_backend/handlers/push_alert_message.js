const path = require('path');
const root = path.parse(process.mainModule.filename).dir;
const express = require('express');
const router = express.Router();
const schedule = require("node-schedule");
const linebot = require('linebot');
const exec = require('exec');
require('custom-env').env();

const handlers = {
    push_message: push_message
};

const bot = linebot({
    channelId: process.env.LINE_BOT_CHANNELID,
    channelSecret: process.env.LINE_BOT_CHANNEL_SECRET,
    channelAccessToken: process.env.LINE_BOT_CHANNEL_ACCESS_TOKEN
});

async function push_message(req, res) {
    res.send(await push_alert_message)
}

async function push_alert_message() {
    bot.push(process.env.ALERT_MESSAGE_USER_ID, ["Warning: React Cryptocurrency backend was down, please check your log file and WAKE UP TO FIX IT!!! ;("]);
    return {"status":true}
}

let rule = new schedule.RecurrenceRule();
rule.second = process.env.SCHEDULE;
let schedule_mission = schedule.scheduleJob(rule, async function(){
    dir = exec("mocha test/test.js", function(err, stdout, stderr) {
        if (err) {
            push_message()
        }
        console.log(stdout);
    });

    dir.on('exit', function (code) {

    });
    console.log("Health Check");
});

module.exports = handlers;
