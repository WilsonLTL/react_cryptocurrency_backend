const path = require('path');
const root = path.parse(process.mainModule.filename).dir;
const express = require('express');
const redis = require('redis');
const axios = require('axios');
const schedule = require("node-schedule");
const redis_se = redis.createClient({port:process.env.REDIS_PORT ,host:process.env.REDIS_URL});
const router = express.Router();

// Redis Coonect Checking
redis_se.on('connect', () => {
    let rule = new schedule.RecurrenceRule();
    rule.second = 1;
    let schedule_mission = schedule.scheduleJob(rule, async function(){
        await update();
        console.log("Data updated")
    });
});

redis_se.on("error", (err)=> {
    console.log("Message: Target node server have no permission to update data")
});

const handlers = {
    update_data: update_data
};

async function update_data(req, res) {
    res.status(200).send(await update())
}

async function update() {
    // update btc-usd
    axios.get('https://api.cryptonator.com/api/ticker/btc-usd').then(function (response) {
        redis_se.lpush("btc-usd",JSON.stringify(response.data));
    }).catch(function (error) {
        return {"status":false,"error":error}
    });

    // update dash-usd
    axios.get('https://api.cryptonator.com/api/ticker/dash-usd').then(function (response) {
        redis_se.lpush("dash-usd",JSON.stringify(response.data));
    }).catch(function (error) {
        return {"status":false,"error":error}
    });

    // update doge-usd
    axios.get('https://api.cryptonator.com/api/ticker/doge-usd').then(function (response) {
        redis_se.lpush("doge-usd",JSON.stringify(response.data));
    }).catch(function (error) {
        return {"status":false,"error":error}
    });

    // update eth-usd
    axios.get('https://api.cryptonator.com/api/ticker/eth-usd').then(function (response) {
        redis_se.lpush("eth-usd",JSON.stringify(response.data));
    }).catch(function (error) {
        return {"status":false,"error":error}
    });

    // update lsk-usd
    axios.get('https://api.cryptonator.com/api/ticker/lsk-usd').then(function (response) {
        redis_se.lpush("lsk-usd",JSON.stringify(response.data));
    }).catch(function (error) {
        return {"status":false,"error":error}
    });

    // update ltc-usd
    axios.get('https://api.cryptonator.com/api/ticker/ltc-usd').then(function (response) {
        redis_se.lpush("ltc-usd",JSON.stringify(response.data));
    }).catch(function (error) {
        return {"status":false,"error":error}
    });

    // update maid-usd
    axios.get('https://api.cryptonator.com/api/ticker/maid-usd').then(function (response) {
        redis_se.lpush("maid-usd",JSON.stringify(response.data));
    }).catch(function (error) {
        return {"status":false,"error":error}
    });

    // update sjcx-usd
    axios.get('https://api.cryptonator.com/api/ticker/sjcx-usd').then(function (response) {
        redis_se.lpush("sjcx-usd",JSON.stringify(response.data));
    }).catch(function (error) {
        return {"status":false,"error":error}
    });

    //update xmr-usd
    axios.get('https://api.cryptonator.com/api/ticker/xmr-usd').then(function (response) {
        redis_se.lpush("xmr-usd",JSON.stringify(response.data));
    }).catch(function (error) {
        return {"status":false,"error":error}
    });

    // update xrp-usd
    axios.get('https://api.cryptonator.com/api/ticker/xrp-usd').then(function (response) {
        redis_se.lpush("xrp-usd",JSON.stringify(response.data));
    }).catch(function (error) {
        return {"status":false,"error":error}
    });
    return {"status":true}
}

module.exports = handlers;