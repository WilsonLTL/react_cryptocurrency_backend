const path = require('path');
const root = path.parse(process.mainModule.filename).dir;
const express = require('express');
const redis = require('redis');
const axios = require('axios');
const redis_se = redis.createClient({port:process.env.REDIS_PORT ,host:process.env.REDIS_URL});
const router = express.Router();

// Redis Coonect Checking
redis_se.on('connect', () => {});

const handlers = {
    xmr_usd: xmr_usd
};

async function xmr_usd(req, res) {
    redis_se.lrange("xmr-usd",0,0, (err, value)=> {
        res.status(200).send(value.toString());
    });
}

module.exports = handlers;