const path = require('path');
const root = path.parse(process.mainModule.filename).dir;
const express = require('express');
const redis = require('redis');
const axios = require('axios');
const redis_se = redis.createClient({port:process.env.REDIS_PORT ,host:process.env.REDIS_URL});
const router = express.Router();

// Redis Coonect Checking
redis_se.on('connect', () => {});

const handlers = {
    xrp_usd: xrp_usd
};

async function xrp_usd(req, res) {
    redis_se.lrange("xrp-usd",0,0, (err, value)=> {
        res.status(200).send(value.toString());
    });
}

module.exports = handlers;