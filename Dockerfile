FROM node:latest
COPY . /code
WORKDIR /code
RUN npm install
RUN npm install pm2 -g
EXPOSE 8080
CMD ["pm2","start","app.js"]