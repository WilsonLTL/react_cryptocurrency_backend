const assert = require('chai').assert;
const axios = require('axios');

let result = "";
describe('Connection Testing', () => {

    it('BTC Unit Testing', function () {
        return Promise.resolve(
            axios.post('http://0.0.0.0:8080/react_cryptocurrency_backend/btc_usd')
                .then(function (res) {
                    assert.ok(res.data["ticker"]["base"] === 'BTC');
                })
        );
    });

    it('ETH Unit Testing', function () {
        return Promise.resolve(
            axios.post('http://0.0.0.0:8080/react_cryptocurrency_backend/eth_usd')
                .then(function (res) {
                    assert.ok(res.data["ticker"]["base"] === "ETH");
                })
        );
    });

    it('LTC Unit Testing', function () {
        return Promise.resolve(
            axios.post('http://0.0.0.0:8080/react_cryptocurrency_backend/ltc_usd')
                .then(function (res) {
                    assert.ok(res.data["ticker"]["base"] === "LTC");
                })
        );
    });

    it('XMR Unit Testing', function () {
        return Promise.resolve(
            axios.post('http://0.0.0.0:8080/react_cryptocurrency_backend/xmr_usd')
                .then(function (res) {
                    assert.ok(res.data["ticker"]["base"] === "XMR");
                })
        );
    });

    it('XRP Unit Testing', function () {
        return Promise.resolve(
            axios.post('http://0.0.0.0:8080/react_cryptocurrency_backend/xrp_usd')
                .then(function (res) {
                    assert.ok(res.data["ticker"]["base"] === "XRP");
                })
        );
    });

    it('DOGE Unit Testing', function () {
        return Promise.resolve(
            axios.post('http://0.0.0.0:8080/react_cryptocurrency_backend/doge_usd')
                .then(function (res) {
                    assert.ok(res.data["ticker"]["base"] === "DOGE");
                })
        );
    });

    it('DASH Unit Testing', function () {
        return Promise.resolve(
            axios.post('http://0.0.0.0:8080/react_cryptocurrency_backend/dash_usd')
                .then(function (res) {
                    assert.ok(res.data["ticker"]["base"] === "DASH");
                })
        );
    });

    it('MAID Unit Testing', function () {
        return Promise.resolve(
            axios.post('http://0.0.0.0:8080/react_cryptocurrency_backend/maid_usd')
                .then(function (res) {
                    assert.ok(res.data["ticker"]["base"] === "MAID");
                })
        );
    });

    it('LSK Unit Testing', function () {
        return Promise.resolve(
            axios.post('http://0.0.0.0:8080/react_cryptocurrency_backend/lsk_usd')
                .then(function (res) {
                    assert.ok(res.data["ticker"]["base"] === "LSK");
                })
        );
    });

});
