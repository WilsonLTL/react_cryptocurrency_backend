# React Cryptocurrency backend
## A micro service for Cryptocurrency
Intro Video: https://youtu.be/aczWMl2YnWg <br >
![image](https://i.imgur.com/xyvZeEJ.png)

### Local File tree
```.
├── Dockerfile
├── README.md
├── .env
├── docker-compose.yml
├── routes
│   └── react_cryptocurrency_backend
│       ├── index.js
│       └── handlers
│           ├── btc_usd.js
│           ├── dash_usd.js
│           ├── doge_usd.js
│           ├── eth_usd.js
│           ├── lsk_usd.js
│           ├── ltc_usd.js
│           ├── maid_usd.js
│           ├── sjcx_usd.js
│           ├── xrp_usd.js
│           ├── xmr_usd.js
│           └── update_data.js
├── package-lock.json
├── package.json
└── app.js
```

### Docker Compose tree
```
├── Nginx
├── Node Server1
└── Scalable Node Server ...
```

## 0. Prepare in your main server
```
1. sudo apt-get update -qy
2. sudo apt install npm -qy
3. sudo apt install docker.io -qy
4. sudo apt install docker-compose -qy
5. docker run -d --name redis -p 6379:6379 redis
```

## 1. Setup the main node server - Ubuntu
```
1. sudo apt-get update -qy
2. sudo apt install npm -qy
3. sudo apt install docker.io -qy
4. sudo apt install docker-compose -qy
5. sudo docker run -d --name redis -p 6379:6379 redis
-------------It should be done in prepareing ------------
6. git clone https://gitlab.com/WilsonLTL/fustily_stress_test.git
7. sudo docker-compose up -d
8. sudo docker exec -it NGINX_CONTAINER_ID /bin/bash
9. apt-get update && apt-get install vim -qy
10. cd /etc/nginx/conf.d
11. vim default.conf
12. Modify all the code in to ##Nginx conf.d
13. sudo docker restart NGINX_CONTAINER_ID
PS1 :##Nginx conf.d in Reference doc
PS2 : Modify the config.js ()
PS3: In Nginx conf.d, cat /etc/hosts and echo ${PORT} to find host and port
```

## 2. API Guide
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/212a539a3f444d93c224) <br >
Url : http://{url}:8888/react_cryptocurrency_backend/btc_usd <br >
Method : POST <br >
Time Cost : 39 ms - 69 ms <br >
```json
{
    "ticker": {
        "base": "BTC",
        "target": "USD",
        "price": "8693.29701378",
        "volume": "87511.15188701",
        "change": "40.53338733"
    },
    "timestamp": 1559055364,
    "success": true,
    "error": ""
}
```

Url : http://{url}:8888/react_cryptocurrency_backend/eth_usd <br >
Method : POST <br >
Time Cost : 39 ms - 69 ms <br >
```json
{
    "ticker": {
        "base": "ETH",
        "target": "USD",
        "price": "269.28815111",
        "volume": "1060615.64151309",
        "change": "1.91882149"
    },
    "timestamp": 1559055423,
    "success": true,
    "error": ""
}
```

Url : http://{url}:8888/react_cryptocurrency_backend/ltc_usd <br >
Method : POST <br >
Time Cost : 39 ms - 69 ms <br >
```json
{
    "ticker": {
        "base": "LTC",
        "target": "USD",
        "price": "114.39188932",
        "volume": "2543223.52305263",
        "change": "1.35670055"
    },
    "timestamp": 1559055423,
    "success": true,
    "error": ""
}
```

Url : http://{url}:8888/react_cryptocurrency_backend/xmr_usd <br >
Method : POST <br >
Time Cost : 39 ms - 69 ms <br >
```json
{
    "ticker": {
        "base": "XMR",
        "target": "USD",
        "price": "96.41828086",
        "volume": "96115.20500682",
        "change": "0.40265965"
    },
    "timestamp": 1559055483,
    "success": true,
    "error": ""
}
```

Url : http://{url}:8888/react_cryptocurrency_backend/xrp_usd <br >
Method : POST <br >
Time Cost : 39 ms - 69 ms <br >
```json
{
    "ticker": {
        "base": "XRP",
        "target": "USD",
        "price": "0.42533837",
        "volume": "501488975.20468998",
        "change": "0.00112100"
    },
    "timestamp": 1559055483,
    "success": true,
    "error": ""
}
```

Url : http://{url}:8888/react_cryptocurrency_backend/doge_usd <br >
Method : POST <br >
Time Cost : 39 ms - 69 ms <br >
```json
{
    "ticker": {
        "base": "DOGE",
        "target": "USD",
        "price": "0.00309787",
        "volume": "268886018.56458998",
        "change": "0.00001000"
    },
    "timestamp": 1559055483,
    "success": true,
    "error": ""
}
```

Url : http://{url}:8888/react_cryptocurrency_backend/dash_usd <br >
Method : POST <br >
Time Cost : 39 ms - 69 ms <br >
```json
{
    "ticker": {
        "base": "DASH",
        "target": "USD",
        "price": "165.95220410",
        "volume": "20261.17684961",
        "change": "2.24053939"
    },
    "timestamp": 1559055543,
    "success": true,
    "error": ""
}
```

Url : http://{url}:8888/react_cryptocurrency_backend/maid_usd <br >
Method : POST <br >
Time Cost : 39 ms - 69 ms <br >
```json
{
    "ticker": {
        "base": "MAID",
        "target": "USD",
        "price": "0.26397134",
        "volume": "",
        "change": "0.00127343"
    },
    "timestamp": 1559053563,
    "success": true,
    "error": ""
}
```

Url : http://{url}:8888/react_cryptocurrency_backend/sjcx_usd <br >
Method : POST <br >
Time Cost : 39 ms - 69 ms <br >
PS: current the api not support since cryptocurrency stop provide service
```json
{
    "ticker": {
        "base": "MAID",
        "target": "USD",
        "price": "0.26397134",
        "volume": "",
        "change": "0.00127343"
    },
    "timestamp": 1559053563,
    "success": true,
    "error": ""
}
```

Url : http://{url}:8888/react_cryptocurrency_backend/lsk_usd <br >
Method : POST <br >
Time Cost : 39 ms - 69 ms <br >
PS: current the api not support since cryptocurrency stop provide service
```json
{
    "ticker": {
        "base": "LSK",
        "target": "USD",
        "price": "2.08971445",
        "volume": "44833.07204926",
        "change": "0.02232477"
    },
    "timestamp": 1559053624,
    "success": true,
    "error": ""
}
```

## Reference doc

### Ubuntu - update nodejs
```
sudo npm cache clean -f
sudo npm install -g n
sudo n stable
```

### Nginx conf.d
** If we got a lots of CPU, we can add worker_processes:YOU_CPU_NUM*2 in to the conf.d <br >
** Increase client_max_body_size and worker_connections to handle more REQUEST !!! 
** For that ^ , modify /etc/nginx/nginx.conf, not /etc/nginx/conf.d/default.conf 
```
upstream node_server {
   server 172.30.0.3:8081;
   server 172.30.0.2:8080;
}
server {
   listen 80;
   server_name localhost;
   location / {
     proxy_pass http://node_server/; 
   }
}
```
```
worker_processes 4;
events { worker_connections: 10240; }
http {
    ...
}
```