const path = require('path');
const root = path.parse(process.mainModule.filename).dir;
const EXPRESS = require('express');
const multer  = require('multer');
const APP = EXPRESS();
const CORS = require('cors');
const HELMET = require('helmet');
const compressor = require('compression');
const axios = require('axios');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
require('custom-env').env();


APP.use(fileUpload());
APP.use(CORS());
APP.use(HELMET());
APP.use(compressor({ threshold: 0 }));
APP.use(bodyParser.urlencoded({ extended: false })); // parse application/x-www-form-urlencoded
APP.use(bodyParser.json()); // parse application/json
APP.set('view engine', 'ejs');
APP.use('/static', EXPRESS.static(__dirname + '/public'));

APP.get('/', function(req, res) {
    res.status(200).send('react_cryptocurrency_backend');
});

APP.get('/loaderio-6ffc1675c4e81d46cde20088979a6021/', function(req, res) {
    res.status(200).send('loaderio-6ffc1675c4e81d46cde20088979a6021');
});

const route_sampleRoute = require(path.join(root, "routes/react_cryptocurrency_backend", "index"));
APP.use(`/react_cryptocurrency_backend`, route_sampleRoute);

let port = process.env.PORT || 8080;

APP.listen(port, () => {
    console.log(`listening port ${port}`);
});
